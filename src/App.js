import './App.css';
import {useState} from "react";
import axios from "axios";
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Checkbox from '@mui/material/Checkbox';
import {FormControlLabel, FormGroup} from "@mui/material";

function App() {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [checked, setChecked] = useState(false);

  const handleChange = (e) => {
    setChecked(e.target.checked)
    localStorage.setItem("checked", e.target.checked)
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    await axios.post("http://localhost:3001/request", {
      email: email,
      password: password,
      checked: checked
    }).then((res) => {
      console.log(res)
    }).catch((err) => {
      console.log(err)
    })
  }

  return (
      <form onSubmit={handleSubmit}>
        <div className="App">
          <Box
              component="form"
              sx={{
                '& > :not(style)': {m: 1, width: '25ch'},
              }}
              noValidate
              autoComplete="off"
          >
            <TextField
                className="text"
                id="outlined-basic"
                label="Email"
                variant="outlined"
                onChange={(e) => setEmail(e.target.value)}
            /><br/>
            <TextField
                className="text"
                id="outlined-basic"
                label="Password"
                variant="outlined"
                onChange={(e) => setPassword(e.target.value)}
            /><br/>
            <FormControlLabel control={<Checkbox/>}
                              label="Remember me"
                              onChange={handleChange}
            />
          </Box>
          <Button type="submit" variant="contained" color="success">
            Success
          </Button>
        </div>
      </form>
  );
}

export default App;
