const express = require("express")
const cors = require("cors")


const PORT = 3001;

const corsOptions = {
    origin: ['http://localhost:3000']
}

const app = express();
app.use(cors(corsOptions))
app.use(express.json())

app.post('/request', (req, res) => {

    res.send(req.body)
    console.log(req.body)

})

app.listen(PORT, () => {
    console.log("running on 3001")
})